import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class pengalamanKerja extends Component {
  render() {
    return (
      <View>
        <View style={{padding:15}}>
            <View style={{flexDirection:'row'}}>
                <View>
                    <Icon size={30} color="black" name={'briefcase-outline'} style={{paddingRight:15}}/>
                </View>
                <Text style={{fontSize:24}}>
                    WORK EXPERIENCE
                </Text>
            </View>
        </View>
        <View style={{padding:15}}>
        <FlatList
            data={
                [
                    {key: '1', perusahaan: 'PUSJATAN Bandung', pekerjaan: 'Junior Software Tester', waktu: '08.2018-02.2019', icon: 'record'},
                    {key: '2', perusahaan: 'MI Cahaya Cimahi', pekerjaan: 'Web Developer', waktu: '2017-2018', icon: 'record'}, 
                    {key: '3', perusahaan: 'HIMA Plano', pekerjaan: 'Web Developer', waktu: '02.2018-03.2018', icon: 'record'}, 
                    {key: '4', perusahaan: 'StartUp jahitku', pekerjaan: 'Web Developer', waktu: '10.2018-11.2018', icon: 'record'}, 
                    {key: '5', perusahaan: 'MI Cahaya Cimahi', pekerjaan: 'Mobile Developer', waktu: '2018-2019', icon: 'record'},  
                ]
            }
            renderItem={({item}) => 
            <View style={{flex:1, width:300  }}>
            <View style={{flexDirection:'row',paddingBottom:15}}>
                <View>
                <Icon size={24} color="black" name={item.icon} style={{paddingRight:15, paddingTop:10}}/>
                </View>
                <View style={{flexDirection:'column'}}>
                    <View >
                        <Text style={{fontSize:18}}>{item.pekerjaan}</Text>
                        <View style={{flexDirection:"row"}}>
                            <Text>
                                {item.perusahaan} 
                            </Text>
                            <Text>
                                ({item.waktu})
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            </View>
            }
            />
        </View>
      </View>
    )
  }
}
