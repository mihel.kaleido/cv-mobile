import React, { Component } from 'react'
import { ScrollView, View, Image, Dimensions, StyleSheet } from 'react-native';
import { BottomNavigation } from 'react-native-paper';

import Pendidikan from './pendidikan';
import PengalamanKerja from './pengalamanKerja';
import Profil from './profil';
import Skill from './skill'

const _ViewPendidikan = ({ route }) => {
  return (
    <Pendidikan/>
  );
};

const _ViewPengalamanKerja = ({ route }) => {
  return (
    <PengalamanKerja/>
  );
};

const _ViewProfil = ({ route }) => {
  return (
    <Profil/>
  );
};

const _ViewSkill = ({ route }) => {
  return (
    <Skill/>
  );
};

export default class App extends Component {

  state = {
    index: 0,
    routes: [
      { 
        key: 'Profil', 
        title: 'Profil', 
        icon: 'photo-album', 
        color: '#6200ee' 
      },
      {
        key: 'Experience',
        title: 'Experience',
        icon: 'work',
        color: '#2962ff',
      },
      {
        key: 'Skill',
        title: 'Skill',
        icon: 'favorite',
        color: '#00796b',
      },
      {
        key: 'Education',
        title: 'Education',
        icon: 'school',
        color: '#c51162',
      },
    ],
  };

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={index => this.setState({ index })}
        renderScene={BottomNavigation.SceneMap({
          Profil: _ViewProfil,
          Experience: _ViewPengalamanKerja,
          Skill: _ViewSkill,
          Education: _ViewPendidikan,
        })}
      />
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 4,
  },
  item: {
    height: Dimensions.get('window').width / 2,
    width: '50%',
    padding: 4,
  },
  photo: {
    flex: 1,
    resizeMode: 'cover',
  },
});
