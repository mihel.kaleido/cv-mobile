import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Skill extends Component {
  render() {
    return (
      <View>
        <View style={{padding:15}}>
            <View style={{flexDirection:'row'}}>
                <View>
                    <Icon size={30} color="black" name={'puzzle-outline'} style={{paddingRight:15}}/>
                </View>
                <Text style={{fontSize:24}}>
                    SKILL
                </Text>
            </View>
        </View>
        <View style={{padding:15}}>
        <FlatList
            data={
                [
                    {key: '1', skill: 'Software Engineering', icon: 'record'},
                    {key: '2', skill: 'Mobile Engineering', icon: 'cellphone-android'}, 
                    {key: '3', skill: 'CodeIgniter', icon: 'record'}, 
                    {key: '4', skill: 'JavaScript', icon: 'language-javascript'}, 
                    {key: '5', skill: 'PHP', icon: 'record'},
                    {key: '6', skill: 'MySQL', icon: 'record'},
                    {key: '7', skill: 'React Native', icon: 'react'},
                    {key: '8', skill: 'Node.js', icon: 'nodejs'},
                    {key: '9', skill: 'Object Oriented Programming', icon: 'record'},  
                ]
            }
            renderItem={({item}) => 
            <View style={{flex:1, width:300  }}>
            <View style={{flexDirection:'row',paddingBottom:15}}>
                <View>
                <Icon size={24} color="black" name={item.icon} style={{paddingRight:15}}/>
                </View>
                <View style={{flexDirection:'column'}}>
                    <View >
                        <Text style={{fontSize:18}}>{item.skill}</Text>
                    </View>
                </View>
            </View>
            </View>
            }
            />
        </View>
      </View>
    )
  }
}
