import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class pendidikan extends Component {
  render() {
    return (
      <View>
        <View style={{padding:15}}>
            <View style={{flexDirection:'row'}}>
                <View>
                    <Icon size={30} color="black" name={'school'} style={{paddingRight:15}}/>
                </View>
                <Text style={{fontSize:24}}>
                    EDUCATION
                </Text>
            </View>
        </View>
        <View style={{padding:15}}>
        <FlatList
            data={
                [
                    {key: '1', education: 'University of Pasundan', tahun:'2014-2018', icon: 'record'},
                    {key: '2', education: 'SMAN 4 Bogor', tahun:'2011-2014', icon: 'record'}, 
                    {key: '3', education: 'SMPN 7 Bogor', tahun:'2008-2011', icon: 'record'}, 
                    {key: '4', education: 'SDN Kota Batu 1 Bogor', tahun:'2002-2008', icon: 'record'}, 
                ]
            }
            renderItem={({item}) => 
            <View style={{flex:1, width:300  }}>
            <View style={{flexDirection:'row',paddingBottom:15}}>
                <View>
                <Icon size={24} color="black" name={item.icon} style={{paddingRight:15, paddingTop:10}}/>
                </View>
                <View style={{flexDirection:'column'}}>
                    <View >
                        <Text style={{fontSize:18}}>{item.education}</Text>
                        <Text>{item.tahun}</Text>
                    </View>
                </View>
            </View>
            </View>
            }
            />
        </View>
      </View>
    )
  }
}
