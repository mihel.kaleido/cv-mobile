/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, FlatList, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
        <ScrollView>
          <View style={{flexDirection:'row'}}>
            <View style={{padding:15}}>
              <Image 
                  source={
                    // uri: 'https://facebook.github.io/react-native/img/opengraph.png',
                    require('./src/images/IMG.png')}
                  style={{
                    width:75,
                    height:75,
                    borderRadius:50,
                  }}    
                />
            </View>
            <View style={{flexDirection:'column'}}>
              <Text style={styles.name}>Helmi Mutawalli Mahir</Text>
              <Text>Code is Art</Text>
            </View> 
          </View>
         
          <View>
          <FlatList

            data={
              [
                {key: 'Address',isi: 'jl.Raya Ciapus (Cimanglid) Gg.Oding no.41 RT.02/RW.12 kec.Tamansari ds.Sirnagalih Bogor Selatan 16610, Bogor, Indondesia', icon: 'map-marker-outline'}, 
                {key: 'Phone',isi: '+62-877-2234-4452', icon: 'cellphone-iphone'},
                {key: 'E-mail',isi: 'helmimutawalli.m@gmail.com', icon: 'email-outline'},
                {key: 'Date of birth',isi: '23-05-1996',  icon: 'calendar'},
                {key: 'LinkedIn',isi: 'linkedin.com/in/mihel-mutawalli-6167285', icon: 'linkedin-box'},
                {key: 'Github',isi: 'github.com/MihelUta', icon: 'github-box'},
              ]
            }
            renderItem={({item}) => 
            <View style={{flex:1, width:300  }}>
              <View style={{flexDirection:'row'}}>
                <View>
                <Icon size={24} color="black" name={item.icon} style={{padding:15,paddingTop:10}}/>
                </View>
                <View style={{flexDirection:'column'}}>
                  <View style={styles.row}>
                  <Text style={{fontSize:18}}>{item.key}</Text>
                  <Text>{item.isi}</Text>
                  </View>
                </View>
              </View>
            </View>
            }
          />    
          </View>
         
      </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  name: {
    fontSize: 20,
    paddingTop: 25,
    textAlign: 'center',
  },
  row: {
    padding: 5,
    paddingLeft: 0,
    marginBottom: 5,
  },
});
